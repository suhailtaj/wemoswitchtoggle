(function() {
    'use strict';

    angular
        .module('myApp', [])
        .controller('homeCtrl', homeCtrl);

    homeCtrl.$inject = ['$http'];

    /* @ngInject */
    function homeCtrl($http) {
        var vm = this;
        vm.activate = activate;
        vm.toggle = toggle;
        vm.msg = "Turn ON Light";
        vm.s = false;
        activate();

        ////////////////

        function activate() {
        }

        function toggle() {
            vm.s = !vm.s;
            if (vm.s == true) {
                vm.msg = "Turn OFF Light";
                $http({
                    method: 'jsonp',
                    url: 'https://maker.ifttt.com/trigger/moter_on/with/key/km9BR_rBcaoO2-3pzcIUE9AVoOzpNyVb4Oh6Vw8qiSz',
                }).then(function(response) {
                });
            } else {
                vm.msg = "Turn ON Light";
                $http({
                    method: 'jsonp',
                    url: 'https://maker.ifttt.com/trigger/moter_off/with/key/km9BR_rBcaoO2-3pzcIUE9AVoOzpNyVb4Oh6Vw8qiSz',  
                }).then(function(response) {    
                });
            }
        }
    }
})();
